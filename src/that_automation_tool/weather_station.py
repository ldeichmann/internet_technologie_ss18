import logging
import threading
import time

import Adafruit_DHT


class WeatherStationHandler:

    def __init__(self, config, mqtt=None):
        self._logger = logging.getLogger(__name__)
        self._mqtt = mqtt
        self._port = config.getint("port")
        self.thread = None
        self.lock = threading.Lock()

    def run_async(self):
        with self.lock:
            if not self.thread:
                self.thread = threading.Thread(target=self._run)
                self.thread.setDaemon(True)
                self.thread.start()

    def _run(self):
        while True:
            try:
                time.sleep(2)
                humidity, temperature = Adafruit_DHT.read_retry(Adafruit_DHT.DHT22, self._port)

                for value, topic, unit in [
                    (humidity, "humidity", "Percent"),
                    (temperature, "temperature", "Celsius"),
                ]:
                    msg = {"value": value, "measurement_unit": unit}
                    self._logger.debug("Publishing message %s on topic %s", msg, topic)

                    self._mqtt.publish(
                        '/sensornetwork/3/sensor/indoor/{}'.format(topic),
                        msg,
                        qos=2, retain=True
                    )
            except (ValueError, KeyError) as e:
                self._logger.error("Malformed input received.")
                self._logger.error(e)
