import threading
import logging
import json

import serial


class WeatherStationArduinoHandler:

    def __init__(self, config, mqtt=None):
        self._logger = logging.getLogger(__name__)
        self.serial = serial.Serial(config['serial_port'])
        self._mqtt = mqtt
        self.thread = None
        self.lock = threading.Lock()

    def run_async(self):
        with self.lock:
            if not self.thread:
                self.thread = threading.Thread(target=self._run)
                self.thread.setDaemon(True)
                self.thread.start()

    def _run(self):
        while True:
            try:
                weather_station_data = json.loads(self.serial.readline().decode('ascii'))
                humidity = weather_station_data['humidity']
                altitude = weather_station_data['altitude']
                temperature = weather_station_data['temperature']
                pressure = weather_station_data['pressure']

                for value, topic, unit in [
                    (humidity, "humidity", "Percent"),
                    (altitude, "altitude", "Meter"),
                    (temperature, "temperature", "Celsius"),
                    (pressure, "pressure", "Pascal"),

                ]:
                    msg = {"value": value, "measurement_unit": unit}
                    self._logger.debug("Publishing message %s on topic %s", msg, topic)

                    self._mqtt.publish(
                        '/sensornetwork/3/sensor/outdoor/{}'.format(topic),
                        msg,
                        qos=2, retain=True
                    )
            except (ValueError, KeyError, UnicodeDecodeError) as e:
                self._logger.error("Malformed serial input received.")
                self._logger.error(e)
            except serial.serialutil.SerialException:
                self._logger.error("No serial device")
