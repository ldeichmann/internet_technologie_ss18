#include <Wire.h>
#include <Adafruit_MPL3115A2.h>
#include <DHT.h>

#include <math.h>

#define DHTPIN 2
#define DHTTYPE DHT22

Adafruit_MPL3115A2 baro = Adafruit_MPL3115A2();

DHT dht(DHTPIN, DHTTYPE);

void setup() {
  Serial.begin(9600);
  dht.begin();
}

void loop() {

  delay(2000);
  
  if (! baro.begin()) {
    Serial.println("Couldnt find sensor");
    return;
  }
  
  float pascals = baro.getPressure();
  float altm = baro.getAltitude();
  float tempC = baro.getTemperature();
  float humidity = dht.readHumidity();
  float tempC2 = dht.readTemperature();

  if (!isnan(tempC2)) {
    tempC = (tempC + tempC2) / 2;
  }

  Serial.print("{");
  Serial.print("\"pressure\": ");
  Serial.print(pascals);
  Serial.print(", \"altitude\": ");
  Serial.print(altm);  
  Serial.print(", \"temperature\": ");
  Serial.print(tempC);
  Serial.print(", \"humidity\": ");
  Serial.print(humidity);
  Serial.println("}"); 
}

