FROM alpine

ADD setup.py /pack/setup.py

ADD README.md /pack/README.md

ADD src /pack/src

RUN apk add --update python3 python3-dev build-base linux-headers && python3 -m ensurepip

RUN ls /pack

RUN cd /pack && python3 -m pip install . --process-dependency-links

CMD [ "python3", "-m", "that_automation_tool.main", "-c", "/etc/tat_config.ini"]
